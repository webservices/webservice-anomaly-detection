cd(joinpath(@__DIR__, "..")); using Pkg; Pkg.activate("."); Pkg.instantiate();
import Chain: @chain
using Flux, CUDA, Transformers, Statistics, CategoricalArrays
using YAML, Dates, Transformers.Basic, Transformers.Stacks, ProgressMeter
##
include("../src/dataloader.jl")
include("../src/transformer_model.jl")

# Parameters
parameters= YAML.load_file("parameters.yaml")
sitename= "lhcbproject"
segmentLength= 15
unknownLabel = 100

# Hyperparams
parameters["transformer"]= Dict(
    "width"=> 512,
)

data, target, vocab= dataloaderTransformerForecasting("webeos/$sitename.jld2",
    segmentLength=segmentLength, batchSize=32, quantizationLevelsPerMetric=[3,4,3,6,4,15,4]) # |> gpu
dimensions=length.(vocab) # |> gpu
targetvocab = collect(1:dimensions[6]) # |> gpu
d_metric= size((data|>first)[1],2)


#"""
#    idx = vec2index(dimensions, vector)
#encodes a n dimensional object to one value
#vec2index([3,4],[1,1]) = 1
#vec2index([3,4],[1,2]) = 2
#vec2index([3,4],[3,4]) = 12
#"""
#function vec2index(dimensions, vector)
#    n = length(dimensions)
#    factor = 1
#    idx = 1
#    for i in collect(n:-1:1)
#        idx += (vector[i]-1)*factor
#        factor *= dimensions[i]
#    end
#    return idx
#end

Size = 100
# Positional encoding - use trigonometric functions
#embed = Embed(Size, prod(dimensions)) # |> gpu
##define a position embedding layer metioned above - somehow use our period to make it circular?
##later will have to do with date time, see implementations in python of similar datasets
#pe = PositionEmbedding(Size) # |> gpu
#
##wrapper for get embedding (includes positional embedding)
#function embedding(x)
#    wordEmbedding = embed(x, inv(sqrt(Size)))
#    wordEmbedding .+ pe(wordEmbedding)
#end
embedding= LinearPositionEmbedding(d_metric, Size)

#try to write encoder, we already have embedded data
encoder = Transformer(Size, 8, 64, 2048) # |> gpu
#N = 2
#encoder = Stack(
#    @nntopo(x → $N),
#    [Transformer(Size, 8, 64, 2048) for i = 1:N]...) # |> gpu

#problem right now, fix later right now just brute force
decoder = TransformerDecoder(Size, 8, 64, 2048) # |> gpu
#decoder = Stack(
#    @nntopo((x,t) → $N),
#    [TransformerDecoder(Size, 8, 64, 2048) for i = 1:N]...)

#define the layer to get the final output probabilities
linear = Positionwise(Dense(Size, length(targetvocab)), softmax) # |> gpu

"""
    encoder_forward(x)

This is the entire "encoder" part of the Transformer. It takes a raw data sample, embeds it, adds positional encoding,
and passes it through the defined number of `Transformer` layers.

Could be implemented with a [`Stack`](https://chengchingwen.github.io/Transformers.jl/dev/stacks/#The-Stack-NNTopo-DSL).
"""
encoder_forward(x) =  @chain x begin
    embedding
    encoder
end

"""
    decoder_forward(x, context)

This is the entire decoder stack. It takes a sequence `x` (or the start of a sequence + UNK symbols if doing iterative prediction)
and a `context` vector, which is normally the output of an encoder stack.
It produces a sequence as output.

Could be implemented with a [`Stack`](https://chengchingwen.github.io/Transformers.jl/dev/stacks/#The-Stack-NNTopo-DSL).
"""
decoder_forward(x, context)= @chain x begin
    embedding
    decoder(_, context)
    linear
end


function smooth(et)
    sm = fill!(similar(et, Float32), 1e-6/size(embeding, 2)) # |> gpu
    p = sm .* (1 .+ -et)
    label = p .+ et .* (1 - convert(Float32, 1e-6)) # |> gpu
end
Flux.@nograd smooth

# Fix the onehot embedding
targetEmbeddingInVocab(target, targetvocab)= @chain target begin
    Flux.onehotbatch(_, targetvocab, unknownLabel) # |> gpu
    # Would be using this one, but the author is enforcing Float32 types and breaks autodiff
    #Flux.Losses.label_smoothing(_, 1e-6)
    smooth
end

"""
    forecastloss(x, target)

Loss function for predicting with a probabilistic Decoder, that doesn't have a paired encoder.
The decoder's output is compared with the distribution of the target across the vocabulary.
The target must be shifted accordingly a priori (the data loader should prepare `train_y` that are shifted as necessary).
"""
function forecastloss(x, target)
    enc = encoder_forward(x)
    predictionDistribution = decoder_forward(x,enc)
    targetDistribution= Flux.onehotbatch(target, targetvocab, unknownLabel)
    #logkldivergence(targetEmbeddingInVocab(target,targetvocab), predictionDistribution)
    Flux.logitcrossentropy(predictionDistribution, targetDistribution)
end



#examples of data and target, target is one ahead of the sample to yield one ahead forecast
dat = [d for d in data][1][1,1] # |> gpu
sample_target = [d for d in target][1][1] # |> gpu
#idx = [vec2index(dimensions,dat[i,:]) for i in collect(1:15)] # |> gpu
enc = encoder_forward(dat)
res = decoder_forward(dat, enc)
los = forecastloss(dat,sample_target)

#batch example
dat2 = [d for d in data][1]
sample_target2 = [d for d in target][1]
#idx2 = represent(dat2)
enc2 = encoder_forward.(dat2)
res2 = decoder_forward.(dat2, enc2)
los2 = forecastloss.(dat2,sample_target2)

#function represent(sample)
#    [[vec2index(dimensions,dat[i,:]) for i in collect(1:15)] for dat in sample]
#end


# Train

# collect all the parameters
trainable_params = Flux.params(embedding, encoder, decoder, linear)
optimizer = ADAM(1e-4)

train = zip([d for d in data], [t for t in target])

#Total loss calculated
function total_loss(train)
    stoploss = 0.0
    for (x,y) in train
        #x = represent(data)
        stoploss += exp(mean(forecastloss.(x, y)))
    end
    return stoploss/length(train)
end

#This is a manual training loop, but it could be simpler using Flux.train!
function train!(epochs)
    @info "Start training with initial loss" total_loss(train)
    for i = 1:epochs
        # Create batches from data
        @showprogress for (x,y) in train
            #x = represent(data)
            #gox, y = todevice(x, y) #move to gpu
            grad = Flux.gradient(()->mean(forecastloss.(x, y)), trainable_params) 
            l = exp(mean(forecastloss.(x, y)))
            @info "Log KL divergence = $l"
            # update parametersbas
            Flux.update!(optimizer, trainable_params, grad)
        end
    end
    @info "Finished with final loss" total_loss(train)
end
