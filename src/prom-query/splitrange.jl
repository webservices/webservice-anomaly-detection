"""
    splitrange(a,b,n,N)

Split range `[a,b]`` with `n` points -> `{[a,x1], [x1+ε,x2], ... , [xn+ε,b]}`
such that each range has <= N points.
Return array of (x1,x2,n).
"""
splitrange(a,b,n,N)= SplitRange(a,b,n,N)|> collect
splitrange(a,b,N; step)= SplitRange(a,b, floor(Int, (b-a) / step) + 1, N)|> collect
splitrange(time, N)= splitrange(
  time["start"][1:end-1]|> DateTime|> datetime2unix,
  time["end"][1:end-1]|> DateTime|> datetime2unix,
  N,
  step= parse(Int, time["step"][1:end-1])
)

"SplitRange produces the next range `(start,stop,maxSplit)` until it has given all `len` points"
struct SplitRange
  start::Float32
  stop::Float32
  len::Int
  maxSplit::Int
end
SplitRange(start,stop,len)= LinRange(start,stop,len)

Base.iterate(r::SplitRange)= iterate(r, SplitRange(r.start, r.stop, r.len))
Base.iterate(r::SplitRange, s)= s.start > s.stop || s.len <= 0 ? nothing : (
    n= min(s.len,r.maxSplit);
    subrangeEnd= s.start + (n-1)*step(r);
    (SplitRange(s.start, subrangeEnd, n),
     SplitRange(subrangeEnd + step(r), s.stop, s.len - n))
  )

step(r::SplitRange)= r.len == 1 ? 0 : (r.stop-r.start) / (r.len-1) #step(LinRange(r.start, r.stop, r.len))
step(r::LinRange)= r.len == 1 ? 0 : (r.stop-r.start) / (r.len-1)
queryTime(r::SplitRange)= queryTime(LinRange(r.start,r.stop,r.len))
queryTime(r::LinRange)= Dict(
  "start"=> (unix2datetime(r.start)|> string) * "Z",
  "end"=> (unix2datetime(r.stop)|> string) * "Z",
  "step"=> (round(Int,step(r))|> string) * "s"
)

Base.length(r::SplitRange)= ceil(Int, r.len / r.maxSplit)