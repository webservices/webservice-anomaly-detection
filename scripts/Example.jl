## Example for using TPA-LSTM
# Use the pkg environment of the repo
# NOTE: Pkg.activate inside the script looks for the filepath relative to the present working directory of the Julia session.
cd(joinpath(@__DIR__, "..")); using Pkg; Pkg.activate("."); Pkg.instantiate();

@info "Loading packages"
using Flux, BSON, GR, TimeSeries, SliceMap, JuliennedArrays
using YAML
using CUDA


## Load libs

include("../src/flux/FluxArchitectures/shared/Sequentialize.jl")
include("../src/flux/FluxArchitectures/shared/StackedLSTM.jl")
include("../src/flux/FluxArchitectures/data/dataloader.jl")
include("../src/flux/FluxArchitectures/TPA-LSTM/TPALSTM.jl")
include("../src/dataloader.jl")

# Global dict with project parameters, such as the data path
parameters= YAML.load_file("parameters.yaml")

# Load some sample data
@info "Loading data"
poollength = 15  #10
horizon = 5  #6
datalength = 100 #2000
file = "webeos/cvmrepo.jld2"
train, test = get_website_data(file, poollength, datalength, horizon) #|> gpu
#train = Float32.(train)
#test = Float32.(test)
#train = [(Float32.(input),Float32.(target)) for (input,target) in train]
#test = [(Float32.(input),Float32.(target)) for (input,target) in test]

# size(input) =(7, 15, 1, 1000)
# size(target) = (1000,)
cd("src/flux/FluxArchitectures/data/")
_input, _target = get_data(:solar, poollength, datalength, horizon) #|> gpu
cd("../../../../")
# size(_input) = (137, 15, 1, 1000)
# size(_target) = (1000,)

## Define the network architecture

@info "Creating model and loss"
inputsize = size(first(train)[1][1],1)
hiddensize = 10
layers = 2
filternum = 32
filtersize = 1

# Define the neural net
model = TPALSTM(inputsize, hiddensize, poollength, layers, filternum, filtersize) #|> gpu

# MSE loss
function lossf(x, y)
    Flux.reset!(model)
    return Flux.mse(model(x'), y')
end

# Callback for plotting the training
# Redo for test set plotting
# Use easier callback fuction in training loop
plotTest = function ()
    Flux.reset!(model)
    pred = model(input)' #|> cpu '
    Flux.reset!(model)
    p1 = plot(pred, label="Predict")
    p1 = plot!(target, label="Data", title="Loss $(loss(input, target))")
    savefig(p1,"tmp-plots/result.png")
end

## Training loop
#Rewrite to using dataloader for a neater for loop
startloss = 0.0
@info "Starting training"
for (input, target) in train
    local lossv = lossf(input, target)
    global startloss += lossv
    @time Flux.train!(lossf, Flux.params(model),Iterators.repeated((input, target), 10), ADAM(0.02))#, cb=cb) #change this to iterator
end
@info "Start loss" startloss/length(train)

Flux.train!(lossf, Flux.params(model), train, ADAM(0.02), cb = () -> println("training"))

#TODO:REDO!!!
#Right now error with calling LSTM due to no 1 layer which is not worth redoing :(

stoploss = 0.0
@info "Finished"
for (input, target) in train
    global stoploss += lossf(input, target)
end
@info "Final loss" stoploss/length(train)

testloss = 0.0
for (input, target) in test
    global testloss += lossf(input, target)
end
@info "Evaluation on test set" testloss/length(test)
