cd(joinpath(@__DIR__, "..")); using Pkg; Pkg.activate("."); Pkg.instantiate();

@info "Loading packages"
import Chain: @chain
using Flux, CUDA, Transformers, Statistics, Plots
using Flux: onehot
using Transformers.Basic #for loading the positional embedding
using Random
g = Random.seed!(0)

#We have our data with 4 different periods of sine
T = collect(0:0.1:31.4)
Sin1 = sin.(T)
Sin2 = sin.(2*T)
Sin3 = sin.(3*T)
Sin4 = sin.(4*T)
Sin = hcat(Sin1,Sin2,Sin3,Sin4) #Dimensions 315x4
savefig(plot(plot(Sin1),plot(Sin2),plot(Sin3),plot(Sin4),layout = 4),"test.png")
Noise = vcat(0.001*randn(g,(Int.(0.75*length(T)*100),4)),randn(g,(Int.(0.25*length(T)*100),4))) # #zeros(Int.(0.75*length(T)*100),4)
Sin_rep = repeat(Sin,100) + Noise #Dimensions 31500x4

#Now we want to do quantization and embedding in intervals
I = collect(-1:0.01:1) #vocabulary
Sin_quant = Int.(round.(100*Sin)) #encoded sample
#The embedding function only works for ints so we scale the 2 dec numbers up



#Positional encoding - use trigonometric functions
#define a Word embedding layer which turn word index to word vector
embed = Embed(512, length(I)) #|> gpu
#define a position embedding layer metioned above - somehow use our period to make it circular?
#later will have to do with date time, see implementations in python of similar datasets
pe = PositionEmbedding(512) #|> gpu
#How to do it when dimensions are time*features and not just words? Not sure it works correctly

#wrapper for get embedding (includes positional embedding)
function embedding(x)
    wordEmbedding = embed(x, inv(sqrt(512)))
    wordEmbedding .+ pe(wordEmbedding)
end


####################################################################
#From here and on maybe the model should be modified - not really sure
####################################################################

#The model, can be used from transformer_model but change things in regards to forecasting
#define 2 layer of transformer
encode_t1 = Transformer(512, 8, 64, 2048) #|> gpu
encode_t2 = Transformer(512, 8, 64, 2048) #|> gpu

#define 2 layer of transformer decoder
decode_t1 = TransformerDecoder(512, 8, 64, 2048) #|> gpu
decode_t2 = TransformerDecoder(512, 8, 64, 2048) #|> gpu

#define the layer to get the final output probabilities
linear = Positionwise(Dense(512, length(I)), logsoftmax) #|> gpu


"""
    encoder_forward(x)

This is the entire "encoder" part of the Transformer. It takes a raw data sample, embeds it, adds positional encoding,
and passes it through the defined number of `Transformer` layers.

Could be implemented with a [`Stack`](https://chengchingwen.github.io/Transformers.jl/dev/stacks/#The-Stack-NNTopo-DSL).

Gives NaN for this case as of now so ot very good.
"""
encoder_forward(x) =  @chain x begin
    embedding
    encode_t1
    encode_t2
end

"""
    decoder_forward(x, context)

This is the entire decoder stack. It takes a sequence `x` (or the start of a sequence + UNK symbols if doing iterative prediction)
and a `context` vector, which is normally the output of an encoder stack.
It produces a sequence as output.

Could be implemented with a [`Stack`](https://chengchingwen.github.io/Transformers.jl/dev/stacks/#The-Stack-NNTopo-DSL).
"""
decoder_forward(x, context)= @chain x begin
    embedding
    decode_t1(_, context)
    decode_t2(_, context)
    linear
end
