ENV["JULIA_DEBUG"] = Main

# Activate packages
cd(joinpath(@__DIR__, "..")); using Pkg; Pkg.activate("."); Pkg.instantiate();
using HTTP, Chain, JSON, YAML, Dates, DataFrames, FileIO
include("../src/prom-query/errorhandling.jl")
include("../src/prom-query/splitrange.jl")
include("../src/prom-query/prometheus-fetch.jl")

# Parameters
clusterName= "webeos"
sitename= "lhcbproject"
timespan= Dates.Day(14)
timestep= "60s"
paramsFile= "parameters.yaml"
datasetDir(clusterName)= joinpath(params["datasetDir"], clusterName)

"""
List of prometheus queries for the given site. Each list entry is a Dict{"name","query"}, giving a name to each query.
  This name will appear as the column name of the resulting data frame.
  Optional `basePeriod` parameter defines the base period over which rates/irates are calculated. If it is set too low, irates will look sparse
  (mostly 0, because no change will have happened in this period).
"""
queries(sitename; clusterName= clusterName, basePeriod= "10m")= begin
  route= "route=\"" * sitename * "." * sitename * "\""
  basePeriod= "[$basePeriod]"
  [Dict("name"=> "connection irate",
    "query"=> """sum without (instance,exported_pod,exported_service,pod,server) (irate(haproxy_backend_connections_total{exported_namespace="$clusterName",$route}$basePeriod))"""
  ),
  Dict("name"=> "incoming throughput",
    "query"=> """sum without (instance,exported_pod,exported_service,pod,server) (irate(haproxy_backend_bytes_in_total{exported_namespace="$clusterName",$route}$basePeriod))"""
  ),
  Dict("name"=> "outgoing throughput",
    "query"=> """sum without (instance,exported_pod,exported_service,pod,server) (irate(haproxy_backend_bytes_out_total{exported_namespace="$clusterName",$route}$basePeriod))"""
  ),
  Dict("name"=> "current sessions",
    "query"=> """sum without (instance,exported_pod,exported_service,pod,server) (haproxy_backend_current_sessions{exported_namespace="$clusterName",$route})""",
  ),
  Dict("name"=> "session irate",
    "query"=> """sum without (instance,exported_pod,exported_service,pod,server) (haproxy_backend_current_session_rate{exported_namespace="$clusterName",$route})""",
  ),
  Dict("name"=> "response latency",
    "query"=> """sum without (instance,exported_pod,exported_service,pod,server) (haproxy_backend_http_average_response_latency_milliseconds{exported_namespace="$clusterName",$route})"""
  ),
  Dict("name"=> "error rate (15m)",
    "query"=> """sum without (code) (sum without (instance,exported_pod,exported_service,pod,server) (rate(haproxy_backend_http_responses_total{code=~"[4|5]xx",exported_namespace="$clusterName",$route}[15m])) ) / """ *
              """sum without (code) (sum without (instance,exported_pod,exported_service,pod,server) (rate(haproxy_backend_http_responses_total{exported_namespace="$clusterName",$route}$basePeriod)) )"""
  ),
  ]
end

## get

params= YAML.load_file(paramsFile)
savePrometheusDataForCluster(clusterName)

## load

ts= load(datasetDir(clusterName) * "/$sitename.jld2")["ts"]
describe(ts)
