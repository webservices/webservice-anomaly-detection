\section{Introduction}
The World Wide Web (WWW) was invented by British scientist Tim Berners-Lee in 1989, while working at CERN \cite{www}. The idea was to share information between scientists in universities and institutes around the world. Over time, web services became increasingly important for CERN for transferring large amounts of data to servers over the world. %TODO: source
The aim of my summer student project was to detect anomalies in some of CERN's web services. 

From Prometheus metrics \cite{prometheus} we have some information about the current state of a web service, like response latency and error rate. We want to avoid a high error rate over a long time period. Moreover the error rate is strongly connected to the response latency of the website: as the latency explodes we might run into errors.

The end goal is to predict upcoming errors or upcoming rapid changes to latency from our metrics. We do this by forecasting the error rate or response latency given the other metrics. It is also interesting to try to classify different states of the data into stable or anomalous states. In this case we have to choose some levels for the different metrics where some levels are treated as anomalous in contrast to stable. We explore different approaches to this problem including time series forecasting, t-SNE visualisation, recurrent LSTM networks and lastly Transformers.

\section{Background}
This section covers the tools and metrics used for the project. Then a survey of previous work in multivariate time series forecasting is given including time series analysis, multivariate time series visualisation, recurrent networks and transformers.

\subsection{Tools}
During this summer internship I gained insights in valuable tools such as working remotely on a Kubernetes cluster, using Kerberos and ssh for authentication and getting better at collaborating with git. I also started managing my references with Zotero. But most of all I started learning a whole new programming language: julia \cite{julia}. I did a tutorial in julia and learned how to use Pluto notebooks (similar to Jupyter). My previous experience of machine learning (ML) was in python using PyTorch so learning julia and ML packages like Flux was interesting. Sometimes when trying to implement papers and posts it would have been more straightforward to use python but in this way we contributed to growing ecosystem of julia packages. 

\subsection{Metrics}
From Prometheus metrics we have some information about the current state of a web service like current sessions, incoming and outgoing throughput, session instantaneous rate and connection instantaneous rate. The current sessions correspond to the number of users currently using the web service. Throughput measures how many packets arrive at their destinations successfully. The session instantaneous rate is the current number of sessions per second over last elapsed second, and the connection rate depends on the internet connection.

Latency describes the amount of delay on a network or internet connection. Low latency implies that there are no or almost no delays. High latency implies that there are significant delays. One of the main aims of improving performance is to reduce latency.

The error rate is the number of times per second we are faced with an error when entering the websites. Often it is undefined or NaN, this is because there are no errors at the time. Then it increases and at some point the error rate is so high so that it is not correctly measured and is stored as Inf. In our data preprocessing we take care of this, so that we do not have NaNs or Infs in our data.

The response latency and the error rate could be correlated, since sometimes high latency can cause errors and the other way around. We aim to detect anomalies in error rate or latency to avoid unexpected errors. However the error rate might experience some periodical effects for some of the websites, for example a brief shutting down period each midnight. Therefore we do not wish to detect just point anomalies of the error rate but times where the error rate is \textit{unexpected}.

\subsection{Time series analysis}
Time series analysis and forecasting can be done using statistical methods without even going into machine learning. The two main methods for time series analysis are frequency-domain methods and time-domain methods, where the former include spectral and wavelet analysis and the latter include auto- and cross-correlation analysis \cite{hyndman2018forecasting}.

An application where spectral analysis is used for Multivariate Time Series (MTS) analysis is in classifying EEG data, for example to classify human emotion \cite{wangEmotionalStateClassification2014,murugappanClassificationHumanEmotion2010}.

Auto- and cross-correlation analysis analysis consists of taking autocorrelation, partial autocorrelation and cross-correlation. Autocorrelation is like correlation but shifted for a series with different lags, and partial autocorrelation is autocorrelation for the difference between two random variables. Cross-correlation is for two different variables \cite{hyndman2018forecasting}. 

There are several different time series models, the autoregressive (AR) models, the integrated (I) models, and the moving average (MA) models. 
These three classes depend \textit{linearly} on previous data points. Combining this give autoregressive moving average (ARMA) and autoregressive integrated moving average (ARIMA) models, or the seasonal autoregressive integrated moving average models (SARIMA) \cite{hyndman2018forecasting}.

However these methods are only linear. Non-linear time series models can represent changes of variance over time (heteroskedasticity). These models represent autoregressive conditional heteroskedasticity (ARCH) and the collection comprises a wide variety of representation (GARCH etc.). These are often used for modelling financial data like the stock market \cite{mcneil2015quantitative}.

\subsection{MTS visualisation}
Several techniques have been developed to visualize and analyze time series data \cite{vanwijkClusterCalendarBased1999,shiRankExplorerVisualizationRanking2012}. Since these techniques are made for univariate time series data they may not fully capture the correlations of the MTS data. However, multivariate data is hard to represent because of the difficulty of mentally picturing data in more than three dimensions. One way of visualising MTS data is Multivariate Time Series t-Distributed Stochastic Neighbor Embedding (m-TSNE) which is a modification of the t-SNE method 
\cite{nguyenMTSNEFrameworkVisualizing2017,wongVisualizingTimeSeries2019}. 
The t-SNE method is similar to Principal Component Analysis (PCA) in reducing dimensions of high-dimensional data and is used for visualisation, with the advantage that t-SNE is non-linear and stochastic compared to PCA which only captures linear dependencies \cite{hintonStochasticNeighborEmbeddinga}. The m-TSNE method first calculates the similarity between all pairs of MTS data points in high-dimensional space, based on Extended Frobenius norm (EROS) which is a similarity metric for MTS data \cite{EROS}. Then, it computes the low-dimensional (2-D or 3-D) projection of the MTS data points using a gradient descent method by preserving the similarity relation between pairs
of high-dimensional points \cite{nguyenMTSNEFrameworkVisualizing2017}.

\subsection{Reccurrent Neural Networks}
The recurrent neural network (RNN) is a common tool in Natural Language Processing (NLP). Recurrent networks have a hidden state that is used for generating a prediction from a sequence. This hidden state is passed on, so that it can be used when generating the subsequent prediction, which gives it a recurrent structure \cite{VanillaRNNsTransformers2020}.

The original RNNs had numerous issues such as the vanishing gradients problem, and no long term memory. The Long Short-Term Memory network (LSTM), which is robust to the vanishing gradients problem, was then developed. Because memory was now maintained separately from the previous cell output it is capable of storing longer-term memory. \cite{VanillaRNNsTransformers2020}

Especially when the attention mechanism was invented on top of the regular LSTM long-term memory issues were diminishing rapidly and good results could be acquired for MTS forecasting. The attention mechanisms contain a weighted context vector that weighs the outputs of all previous prediction steps instead of the hidden state \cite{shihTemporalPatternAttention2019}.

It is also possible to combine convolution with LSTMs and use it for similar MTS forecasting and classification tasks  \cite{zhangResearchGasConcentration2019,waczynskaConvolutionalLSTMModels2021}.
However, the convolutional neural nets were more focused on image processing application such as pedestrian identification \cite{liAttentionBasedCNNConvLSTM2020}.

LSTMs still had some issues since processing had to be performed sequentially, imposing a significant resource bottleneck. This problem was solved when the Transformer was introduced. The Transformer is capable of maintaining the attention mechanism while processing sequences in parallel: all words together rather than on a word-by-word basis. Transformers also make it possible to transition from point forecasting to a sequence-to-sequence model~\cite{vaswani2017attention}.

\begin{figure}[h!]
    \centering
    \includegraphics[width = 0.5\textwidth]{tmp-plots/transformers.png}
    \label{fig:transformers}
    \caption{The transformer architecture. \cite{vaswani2017attention}}
\end{figure}

 \newpage
\subsection{Transformers}
Transformers consist of an encoder segment, which 
\begin{enumerate}
    \item takes inputs from the source language, 
    \item generates an embedding for them, 
    \item encodes positions, 
    \item computes where each word has to attend to in a multi-context setting, 
    \item outputs some intermediary representation.
\end{enumerate}

The next part is a decoder segment, which takes inputs from the target language, generates an embedding for them with encoded positions, computes where each word has to attend to, and subsequently combines encoder output with what it has produced so far. The outcome is a prediction for the next token, by means of a Softmax and hence argmax class prediction (where each token, or word, is a class) \cite{IntroductionTransformersMachine2020}.

The trick with the transformer to handle the sequence is the positional encoding which stores positions in the sequence as values. This time we do not have a strict ordering but the position is still taken into account due to the encoding. The other thing is the attention mechanism in the encoder and decoder, which chooses which elements in the sequence to focus on \cite{IntroductionTransformersMachine2020}.

Transformers have had great success in NLP with large language models such as GPT-2, GPT-3, BERT and so on. But how do we use them for MTS data? Some ideas include Adversarial Space Transformer for time series forecasting  \cite{wuAdversarialSparseTransformer2020} and Gated Transformer Networks for MTS classification \cite{liuTransformerNetworks2021}. 


\section{Method}
The methods used for our project is described from preprocessing and data analysis to our implementations of TPA-LSTM and transformers. The code for the project can be found on gitlab \url{https://gitlab.cern.ch/webservices/webservice-anomaly-detection/}.

\subsection{Preprocessing}
Data was collected from around 2000 web services using the prometheus metrics over the scope of two weeks. This formed a MTS with 7 metrics (features) over the time of 20064 minutes. First a significance metric is calculated for all the website data, excluding the ones with too little traffic to be interesting. The metric is simply the sample variance of the connection rate, and if it is too low the data is discarded. This narrowed down the number of websites to around 100, from where we choose to focus on those with most activity, for example \texttt{lhcbproject} and \texttt{cvmrepo}.

In the error rate we substituted NaNs to zeros and Infs to max of the data. The other metrics had no missing data so we left this unprocessed. We used this data in the data analysis such as autocorrelation functions. For the rest of the data analysis the data was then mean-centered and min-max normalized. 

\subsection{Data Analysis}

The time series properties of the relevant websites were analyzed with autocorrelation and partial autocorrelation plots as well as crosscorrelation plots. We noticed slight seasonality for the error rates but no clear trend. Overall the data did not look like the ARIMA model could explain it; it was quite irregular and occasionally had high anomalous spikes. We did not notice any significant correlation between the response latency/error rate with the other metrics. This means that regular time series forecasting would not be feasible and this is likely due to non-linear dependency. 

We tried out some periodogram and spectrogram techniques but they were hard to interpret. The data was also visualised with m-TSNE plots, to try to find clusters in the data. However the representation of the data and implementation of the m-TSNE method proved to be quite tricky so we did not get any good results. 

\subsection{Temporal Pattern Attention Long Short-Term Memory Network (TPA-LSTM)}
For the TPA-LSTM we based our code on the example on github \url{https://sdobber.github.io/FA_TPALSTM/}, which was already made for MTS forecasting for solar power data. Similarly to the solar power data we segmented the data into windows that are overlapping (stride = 1). In our case the windows were 15 minutes long. We simultaneously stored the target value to be predicted, a horizon ahead. We choose the horizon to be 5 minutes. So from the 15 minute window we wanted to do a point prediction of the error rate 5 minutes into the future. Iterated prediction is a possibility but we did not delve into that. 

In practice we reshaped our $20064\times7$ array to some batches of $7\times15\times1\times100$ where 100 was our batchsize. The extra dimension was added for practicalities when using a LSTM.

\subsection{Transformer for MTS forecasting}
For implementing Transformers we followed the tutorial \cite{TransformerImplementationTimeSeries} and the paper \cite{liuTransformerNetworks2021}. We used the Transformers package in julia.

We created levels for discretization for each metric, to mimic a vocabulary. When used for NLP purposes, Transformers have a vocabulary with all the words and the output is given as a probability distribution of all the words in the vocabulary. However when we have a MTS instead of a word sequence, we do not have a vocabulary (that would be all the real numbers, an infinite set). Thus we created a discretized MTS with between 3 and 15 levels per metric depending on the metric (more details can be found in the code). The value at a given timestamp was in a given class if the current value is beneath that level but above the previous one. 

The goal for the transformer was to predict the next minute's response latency class given the classes of all the metrics in the current 15 minute segment. The segments were stored in batches of size 64. We used Cross Entropy loss and trained about 10 epochs which took around 10-20 minutes. (Training on GPU was attempted but had numeric instability).

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.3\textwidth}
        \centering
        \includegraphics[width = \textwidth]{tmp-plots/data100_1.png}
        \caption{1 iteration.}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \centering
        \includegraphics[width = \textwidth]{tmp-plots/data100_5.png}
        \caption{5 iterations.}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \centering
        \includegraphics[width = \textwidth]{tmp-plots/data100_100.png}
        \caption{100 iterations.}
    \end{subfigure}
    \caption{Results from running different iterations of training of TPA-LSTM on 100 minutes of website data from cvmrepo.}
    %**TODO I cannot see the blue - too tiny graph
    \label{fig:res100}
\end{figure}

The positional encoding was standard for transformers and for the embedding we used a linear embedding layer. We used a single decoder for simplicity and repeated it 2 times. More implementation details can be found in the code.




\section{Results}
Here we present results for TPA-LSTM and Transformers training.



\subsection{TPA-LSTM}
When we tried TPA-LSTM we could only do small segments at a time, since it took a lot of time, and the processor ran out of memory. In figure \ref{fig:res100} we see how the network learns for a 100 minute segment for the \texttt{cvmrepo} data. In figure \ref{fig:res1000} we see it corresponding for 1000 minutes. Unfortunately this is too short of an interval to see the clear daily dependence, which might have been easier to learn. We see quite good results regardless but it is important to keep in mind that it is only evaluated on training data so it could be overfitting. 


\begin{figure}[hbtp]
    \centering
    \begin{subfigure}[b]{0.6\textwidth}
        \centering
        \includegraphics[width = \textwidth]{tmp-plots/data1000_10.png}
        \caption{10 iterations.}
    \end{subfigure}
    
    
    \begin{subfigure}[b]{0.6\textwidth}
        \centering
        \includegraphics[width = \textwidth]{tmp-plots/data1000_100.png}
        \caption{100 iterations.}
    \end{subfigure}
       \caption{Results from running different iterations of training of TPA-LSTM on 1000 minutes of website data from cvmrepo. The label to be predicted was the error rate 5 minutes into the future.}
       \label{fig:res1000}
\end{figure}

\subsection{Transformers}
The transformers training was much more efficient, so we could train the whole interval. We also changed to another website data (lhcbproject) and changed from error rate to response latency. Also our data was discretized as opposed to raw. This makes the two plots look quite different but also here we get a quite good result, even if it is hard to see. Again a problem is that we are evaluating on the training set and not a test set. 
\begin{figure}[h!]
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width = \textwidth]{tmp-plots/prediction_total_epoch1.png}
        \caption{Epoch 1.}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width = \textwidth]{tmp-plots/prediction_total_epoch10.png}
        \caption{Epoch 10.}
    \end{subfigure}
       \caption{Results from running different iterations of training of Transformer on 20064 minutes of website data from lhcbproject. The label to be predicted was the response latency class 1 minute into the future.}
       \label{fig:res}
\end{figure}

\section{Discussion}
There is a lot of room for improvement for this project. First of all, we only focused on two of the websites and undeniably it would be interesting to also examine the other websites. For example, if we would find some interesting conclusion in the forecasting, it would be interesting to see if this also holds for other websites - so that the whole web server is affected, or if it is largely on a website to website basis. 

Regarding the visualisation it would be interesting to see what clusters could be found with m-TSNE if we got it to work, because it seems promising. 

The project was carried out in quite an explorative manner: we were not sure which method to use at first and only learned about Transformers at the end of the project. There is surely possible to get improvements in the TPA-LSTM part but I think Transformers are the way to go in this problem area.

We did not have time for hyper-parameter tuning, or separate training and test set for either TPA-LSTM or Transformers. This would surely improve the results a lot. It is also a good idea to vary the size of the segment from 15 to something else and to vary the horizon from 5 minutes in the TPA-LSTM. For the Transformer we had one-ahead prediction and it would be more interesting for a practical application to predict a larger window.

The Transformer architecture could be varied a lot, with different combinations of decoder and encoders and changing the embedding, positional encoding and segmentation. We choose to have levels in the Transformers but how would the result be if we removed them? Or if we had even cruder classes like anomalies or not.

Lastly, our data is sparse and this might affect the training results. Maybe special methods can be applied for sparse data.

\subsection{Conclusion}
The TPA-LSTM is a good model for forecasting error rate but not efficient enough. Therefore we use transformers for classifying response latency. This could be expanded for more data and by doing a more proper training. 

\section{Acknowledgements}
I am thankful to CERN for giving this amazing opportunity to participate in the Summer Student Programme, including people from all over the world, great lectures and an interesting project. A big thank you to Konstantinos Samaras-Tsakiris for helping me in every step of the way going through with this project. I also want to thank Sofia Vallecorsa for providing helpful insights.
