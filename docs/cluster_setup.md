# K8s cluster setup

Goal: a cluster where we can do webservice analysis.

Requirements:
- [EOS](#eos) access to store datasets
- [GPU](#gpu)
- [Default StorageClass](#defsc)

!!! note
    These requirements should be satisfied at cluster setup by the cluster admin;
    no need for any action by the developer.

## <a id="eos"/>EOS

Cluster has EOS access by default; the container should mount the mountpoints from the host.

[Reference workload](https://gitlab.cern.ch/helm/charts/cern/raw/master/eosxd/examples/eos-client-example.yaml):

```yaml
containers:
	volumeMounts:
		- name: eos
			mountPath: /eos
			mountPropagation: HostToContainer
volumes:
	- name: eos
		# Note the particularity of the host path being /var/eos, not /eos.
		# The path inside the container will be /eos with this sample setup.
		hostPath:
			path: /var/eos
securityContext:
	seLinuxOptions:
		type: "spc_t"
```

## <a id="gpu"/>GPU

Openstack project `IT Drupal Hosting Service - Development` has access to VM flavor `g2.xlarge`.
Theoretically, that should be enough to access a GPU.
However, due to [INC2747996](https://cern.service-now.com/service-portal?id=ticket&n=INC2747996), right now we can't consume it.

The GPU should be [exposed as a node resource](https://kubernetes.io/docs/tasks/administer-cluster/extended-resource-node/) by Magnum
and pods will access it simply by [adding a resource request](https://v1-17.docs.kubernetes.io/docs/tasks/configure-pod-container/extended-resource/):

```yaml
resources:
	requests:
		nvidia-gpu: 1  # replace resource name with what is advertised by the node
```

## <a id="defsc"/>Default Storage Class

There's 2 storage classes:
- `meyrin-cephfs`
- `geneva-cephfs-testing`

```bash
kubectl patch storageclass geneva-cephfs-testing -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```
