"`retryServerErrors(httpreq; maxDelay=10)` retries the HTTP request as long as it responds with status >= 500"
retryServerErrors(httpreq; maxDelay=4)= retry(httpreq;
  delays= Base.ExponentialBackOff(n=4, first_delay=1, max_delay=maxDelay),
  check= (delay, httpErr::HTTP.StatusError) -> begin
    @debug "Retrying server error" httpErr
    delay[1] <= maxDelay && httpErr.status >= 500
  end)

"""
    retry!(f;  delays=ExponentialBackOff(), check! = nothing)

Like [`retry`](@ref), but `check!` can mutate the arguments received by `f` based on the error conditions.

# Examples

```
retry!(f; check! = nothing)
````
"""
function retry!(f;  delays=ExponentialBackOff(), check! = nothing)
    (args...; kwargs...) -> begin
        y = iterate(delays)
        while y !== nothing
            (delay, state) = y
            try
                return f(args...; kwargs...)
            catch e
                y === nothing && rethrow()
                if check! !== nothing
                    result = check!(state, e, args...; kwargs...)
                    state, retry_or_not = length(result) == 2 ? result : (state, result)
                    retry_or_not || rethrow()
                end
            end
            sleep(delay)
            y = iterate(delays, state)
        end
        # When delays is out, just run the function without try/catch
        return f(args...; kwargs...)
    end
end