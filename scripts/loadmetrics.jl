#This file is run for filtering out the interesting websites, and produces a data.jld2 file containing these.
include("../src/loadfile.jl")

metric(df :: DataFrame) = mean_and_var(df[!,"connection irate"])
metric(m :: Missing) = (-1.0,-1.0)
metric(m :: Nothing) = (-2.0,-2.0)

d = Dict{String, Tuple{Float64, Float64}}()
for (dir, root, files) in walkdir("/home/jovyan/webeos")
    for file in files
        println(file)
        df = load_website(file)
        d[file] = metric(df)
    end
end

save("data.jld2", "data", d)
tol = 0.2
significant = filter(x -> last(x)[2] > tol, d)
